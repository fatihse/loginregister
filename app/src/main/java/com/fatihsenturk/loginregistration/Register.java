package com.fatihsenturk.loginregistration;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

/**
 * Created by TOSHIBA on 28.11.2015.
 */
public class Register extends Activity {

    private EditText username;
    private EditText email;
    private EditText password;
    private EditText passwordAgain;
    private Button registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        username = (EditText) findViewById(R.id.registerName);
        email = (EditText) findViewById(R.id.registerEmail);
        password = (EditText) findViewById(R.id.registerPassword);
        passwordAgain = (EditText) findViewById(R.id.registerPasswordAgain);
        registerButton = (Button) findViewById(R.id.registerScreenButton);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                String currentUsername = username.getText().toString().trim();
                String currentEmail = email.getText().toString().trim();
                String currentPassword = password.getText().toString().trim();
                String currentPasswordAgain = passwordAgain.getText().toString().trim();

                if (!currentPassword.equals(currentPasswordAgain)) {
                    Toast.makeText(Register.this, "Sifreler ayni olmali", Toast.LENGTH_SHORT).show();
                    return;
                } else if (currentUsername.length() == 0 || currentEmail.length() == 0 || currentPassword.length() == 0 || currentPasswordAgain.length() == 0) {
                    Toast.makeText(Register.this, "Lutfen tum alanlari doldurunuz", Toast.LENGTH_SHORT).show();
                    return;
                } else {

                    final ProgressDialog progressDialog = ProgressDialog.show(Register.this, null, "Lutfen Bekleyin..");
                    ParseUser parseUser = new ParseUser();

                    parseUser.setUsername(currentUsername);
                    parseUser.setEmail(currentEmail);
                    parseUser.setPassword(currentPassword);

                    parseUser.signUpInBackground(new SignUpCallback() {
                        @Override
                        public void done(ParseException e) {
                            progressDialog.dismiss();
                            if (e == null) {
                                Toast.makeText(Register.this, "Basarili bir sekilde kayit oldunuz..", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(Register.this, MainActivity.class));
                            } else {
                                Toast.makeText(Register.this, "Kayit sirasinda bir sorun olustu:" + e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                }
            }
        });

    }
}
