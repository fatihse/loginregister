package com.fatihsenturk.loginregistration;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

/**
 * Created by TOSHIBA on 28.11.2015.
 */
public class LoginRegistrationApplication extends Application {
    /*
    * App id from parse.com, dashboard -> settings -> key
    * Same for client key
    * */
    @Override
    public void onCreate() {
        super.onCreate();
        Parse.initialize(this, getString(R.string.APP_ID), getString(R.string.CLIENT_KEY));
        ParseUser.enableRevocableSessionInBackground();
        ParseInstallation.getCurrentInstallation().saveInBackground();
    }
}
