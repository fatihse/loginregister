package com.fatihsenturk.loginregistration;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.parse.ParseUser;

/**
 * Created by TOSHIBA on 28.11.2015.
 */
public class Dispacher extends Activity {

    public Dispacher() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (ParseUser.getCurrentUser() != null) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, LoginRegistration.class);
            startActivity(intent);
        }
        super.onCreate(savedInstanceState);
    }
}
